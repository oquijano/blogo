blogo
=====

Simple blog that supports an interface with multiple languages.

## Installation

You need [racket](https://racket-lang.org/) installed in your
computer. You can then install blogo by running the following command

```sh
raco pkg install  https://gitlab.com/oquijano/blogo.git
```

Add your racket add-ons launcher folder to your path. You can see what
this folder is by running the following command

```sh
racket -e "(path->string (build-path (find-system-path 'addon-dir) (version) \"bin\"))"
```

## Getting Started

After installing blogo you should have two new commands available in
your terminal: `blogo` and `blogo-client`. The first one is used on
the server side to start the blog. The second one on the client side
to connect to the server api and add, remove or modify posts or post
previews.

### Copying blog files to a folder

Create an empty folder and enter it with your terminal. Then run the
command
```sh
blogo copy-files
```

This will copy the following files to the folder

* *main\_page\_template.html* : Template used for the main page of the blog
* *post_template.html* : Template used for the posts
* *static\_pages\_template.html* : Template used for the static pages
* *htdocs* : Folder for adding css, images or any kind of file one needs
  to reference from the blog website. 
* *init.yml* : Configuration file used to set up the blog. It is only
  used the first time the blog runs.
  - languages : The list of languages for the web interface
  - title : The title of the blog for each language
  - go\_back\_message : The "Go back to main page" massage for each language
  - static_pages : Information about the static pages to include. Each
    static page should be followed by all the languages and the
    contents one wants on each static page.
* *config.yml* : General configuration file of the blog.



The .html files are templates parsed by racket to generate the
web-pages of the blog. You can edit them or add/modify the css to
personalize them in any way you want. Just be careful with the parts
containing @'s, this is racket code that generates html. If you wanna
change that too, you need fo familiarize with the
[@syntax](https://docs.racket-lang.org/scribble/reader.html) of
[scribble](https://docs.racket-lang.org/scribble/index.html). 

### Configuration files

There are two configuration files you have to customize: *init.yml* and
*config.yml*.

#### init.yml

This file is only used the first time the blog starts. It sets
the languages, go back message and static pages. In the init file,
each static page is defined by a label that is not included in the
blog. Then for each language in the blog there should be a
corresponding title and body of the static page. Both the body and the
title can have html tags in them. [In this
page](https://yaml-multiline.info/) you can see several options to
have multiline strings in yaml files.

#### config.yml

The blog block has the following parameters

* **port** : port that serves the blog website.
* **db_file** : path to a sqlite file where the website information
  will be stored. It it does not exist it will be created.
* **root_folder** : Path to the folder containing the html templates
  and htdocs folder. It can either be an absolute path or a relative
  path with respect to the folder containing the *config.yml* file.
* **posts_per_page** : Maximum number of posts to show on the
  page. When there exist more posts than this, navigation buttons will
  appear to go to the next or previous posts.
* **default_language** : Language to use for the website when
  preferred languages provided by the browser do not match any of the
  languages provided by the browser.

The api block contains the following parameters

* **enable** : Whether to enable the api system so clients can connect
  to it and do changes to the blog.
* **port** : The port on which the api system will run if it is enabled.

The initial_config parameter is the path to the *init.yml* file to use
for the blog.

### Starting the blog

Once *config.yml* and *init.yml* have been personalized, the blog can
be started by running the following command in the folder where the
files were copied.

```
blogo start
```

This will start listening on two different ports. One port serves the
blog website and the other one the API for modifying the contents of
the blog. Assuming you did not change the default ports in
*config.yml* the website is served on port 8000 and the api on port
8001. So, at this point you can see the server website by visiting
[http://localhost:8000](http://localhost:8000).


### Using the client to update the blog

The command `blogo-client` modifies the content of the blog by
connecting to the API. It needs to know the port on which the API is
listening. This can be done by either setting the environment variable
`BLOGO_PORT` or by using the `-p` option to `blogo-client`. In unix
like operating systems you can set the value of `BLOGO_PORT` by typing

```
export BLOGO_PORT=8001
```

On windows you can follow [these
instructions](https://www.techjunkie.com/environment-variables-windows-10/). If
you find that too cumbersome remember you can always simply use the
`-p` option instead.

To see a list of all the operations that can be done with
`blogo-client` you can run
```
blogo-client --help
```


Let us now add a post. Create a file called *my_first_post.html* with the
following contents

```
Hello world!
</br>
Here is a list:
<ul>
  <li> First element </li>
  <li> Second element</li>
</ul>
```

Note that in the file you only need to put the body of the post. 

Let us now add a post with the contents of *my_first_post.html* with
the title *First post*. If you did not set the environment variable
`BLOGO_PORT` you can do this with the command

```
blogo-client -p 8001 add-post "First post" my_first_post.html
```

otherwise you can simply do

```
blogo-client add-post "First post" my_first_post.html
```


Go know to the blog website and see that a new post has appeared with
the contents of *my_first_post.html*. Note that in the url path of the
post you have a part with "id/1?***". The number that comes after id
(in this case 1) is the id of the post. You need the post id to edit
or remove an existing post. 

If you want to remove the post we just added you do 

```
blogo-client -p 8001 rm-post 1
```

of if you did set the variable `BLOGO_PORT`:
```
blogo-client  rm-post 1
```


To edit a post you can use the commands `fetch-post-body` and
`update-post`. Run `blogo-client --help` to see how the arguments
required y this commands.

From now on it is assumed that the environment variable `BLOGO_PORT`
has ben set if this is not your case, remember to use the option `-p`
of `blogo-client`.

Besides of posts, one can create previews to see how the post would
look like. Let's use again the file we created before, we can see it
as a preview with the command 

```
blogo-client add-preview "My first preview" my_first_post.html
```

After that you can see the preview at the address
[http://localhost:8001/preview/1](http://localhost:8001/preview/1).
Note that the port used with the localhost in this case is the API
port.

You can see a list of all the current previews with 
```
blogo-client list-previews
```

in there you are going to see first a number which is the preview and
id and it is followed by the preview title. Once you have uploaded a
preview you can erase it or publish it as a post. For this you can use
the commands `rm-post` and `publish-preview` of `blogo-client`. 


### Using the client with a remote server

In our examples so far the server and the client were running on the
same computer. Let us now suppose that the blog is running on a remote
server and you want to connect the client to it.

In the current version of the program you have to create a tunnel to
the API port in the remote computer. This can be done securely using
`ssh`. I assume here you have ssh access to the remote computer, let's
suppose *user* is your username and *remotecomputer.com* is a url that
connects to the remote computer. If the API is running on the default
port, you can run the following command to start a tunnel

```
ssh -fNL 8001:localhost:8001 user@remotecomputer.com
```

after doing this you can assign 8001 to `BLOGO_PORT` and use all the
commands we have seen so far normally. 

In the current state of the program, if another user has access to the
computer, they can connect to the API port and make modifications to
the running blog. Security measures will be taken in future versions
to prevent this.
