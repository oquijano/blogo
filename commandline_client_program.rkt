#lang racket/base

(require racket/cmdline
	 racket/port
	 json
	 blogo/updateapi
	 racket/format
	 net/http-client
	 net/uri-codec
	 racket/file
	 racket/match
	 racket/math
	 racket/string)


(define port (make-parameter null))
(define host (make-parameter "localhost"))
(define js-files (make-parameter null))
(define css-files (make-parameter null))

(define (run-command)
  (cond
   [(not (port)) (displayln "Please provide a port or set the variable BLOGO_PORT") ]
   [(not (and (integer? (port)) (positive? (port)) (< (port) 65535) )) (displayln (format "~a is not a valid port number" (port)))]
   ;[#t (displayln (format "port: ~a" (port)))]
   ))

(define (print-previews id-title-pairs)
  (let auxf ([pairs-list id-title-pairs])
    (when (not (null? pairs-list))
	(begin
	  (displayln (string-append
		      (~a (caar pairs-list)  #:width 3 #:align 'right)
		      (format ": ~a" (cadar pairs-list))))
	  (auxf (cdr pairs-list))))))

(define (post-and-print path data)
  ;; Sends a post request and prints the response body
  (define-values (status headers body-in)
    (http-sendrecv (host) path
			   #:port (port)
			   #:data (alist->form-urlencoded data)
			   #:method #"POST"
			   #:headers (list "Content-Type: application/x-www-form-urlencoded")))
  
  (displayln (port->string body-in)) )


(define (string->positive-integer x)
  ;; Returns the positive integer contained in the string x
  ;; If the contents of x are not a positive integer it returns #f

  (let/cc return
	  (if (string? x)
	      (let ([number (string->number x)])
		(if (positive-integer? number)
		    (return number)
		    (return #f)))
	      (return #f))))

(define (create-headers headers-list format-string)  
  (apply string-append (map (lambda (x) (format format-string x))
	headers-list
	)))

(define (js-headers)
  (create-headers (js-files) "<script src=\"~a\"></script>")
  )

(define (css-headers)
  (create-headers (css-files) "<link rel=\"stylesheet\" href=\"~a\" type=\"text/css\"/>")
  )


(define cmdline-function
  (command-line
   #:multi
   [("--add-js") js-path "Path (in the server) to a javascript file to be included in the header of a post or preview." (js-files (append (js-files) (list js-path)))]
   [("--add-css") css-path "Path (in the server) to a css file to be included in the header of a post or preview." (css-files (append (css-files) (list css-path)))]
   #:once-each
   [("-p" "--port") p "Local port to use for the client. If not provided it uses the value of the environment variable BLOGO_PORT"
    (port (string->positive-integer p))]
   #:usage-help
   "    add-preview : Adds a preview."
   "                  Arguments:"
   "                  title : Title of the post"
   "                  path : Path to a text file with the contents of the preview"
   "  list-previews : Shows a list of the current previews with their ids."
   "     rm-preview : Removes a preview."
   "                  Argument:"
   "                  id: The id of the preview to be removed"
   "        rm-post : Removes a post."
   "                  Argument:"
   "                  id: The id of the post to be removed"
   "       add-post : Adds a post"
   "                  Arguments:"
   "                  title : The title of the post"
   "                  path : Path to a text file with the contents of the post"
   "                  date (optional) : Date attached to the post. If not provided the current date will be used."
   "fetch-post-body : Gets the body of a published post."
   "                  Arguments:"
   "                  id : The id of the post to obtain"
   "                  filename (optional) : The filename where the body will be stored. If it is  not provided it will be printed to the standard input"
   "    update-post : Replaces an existing post"
   "                  Arguments:"
   "                  id : The id of the post to edit"
   "                  filename : Name of the file with the text to replace the existing file"
   "                  new_title (optional) : A string with the title of the new edited post. It it is not provided the title will be the same than the existing post"
   "publish-preview : Creates a post with the contents of an existing preview. The preview is erased."
   "                  Arguments:"
   "                  id : The id of the preview to publish"
   #:args (command . args)

   (cond
    [(not (port))
     (raise-user-error "The value passed to -p must be a positive integer")]
    [(null? (port))
     (let ([port-env (string->positive-integer (getenv "BLOGO_PORT")) ])
       (if port-env
	   (port port-env)
	   (raise-user-error "Please provide a valid port either using the -p option or through the environment variable BLOGO_PORT")))])

   (cond
    
    
    [(string=? command "add-preview")
     
     (with-handlers [(exn:fail:contract? (lambda (exn)
					   (displayln "add-preview needs two arguments.")))]	 
	 (let ([title (car args)]
	       [body-path (cadr args)]
	       [headers (string-append (js-headers) (css-headers))])
	   (if (string=? headers "")
	       (post-and-print "add-preview"  `((title . ,title)
						(body . ,(file->string body-path))))
	       (post-and-print "add-preview"  `((title . ,title)
						(body . ,(file->string body-path))
						(headers . ,headers))))
	   ))]

    [(string=? command "list-previews")
     (define-values (status headers body-in)
       (http-sendrecv (host) "/list-previews"
		      #:port (port)))
     
     (print-previews (string->jsexpr (port->string body-in)))]

    [(string=? command "rm-preview")
     (let* ([id (car args)])
       (if id
	   (post-and-print  "rm-preview" `((id . ,id)))	   
    	   (displayln "Error: id must be a number")	   
    	   ))]

    
    [(string=? command "add-post")

     (define headers (string-append (js-headers) (css-headers)))
     
     (match args
       [(list title body-path)
	(if (string=? headers "")
	 (post-and-print "add-post" `((title . ,title)
				      (body . ,(file->string body-path))))
	 (post-and-print "add-post" `((title . ,title)
				      (body . ,(file->string body-path))
				      (headers . ,headers))))]
       [(list title body-path post-date)
	(if (string=? headers "")
	    (post-and-print "add-post"  `((title . ,title)
					  (body . ,(file->string body-path))
					  (date . ,post-date)))
	    (post-and-print "add-post"  `((title . ,title)
					  (body . ,(file->string body-path))
					  (date . ,post-date)
					  (headers . ,headers)
					  )))
	])]

    [(string=? command "fetch-post-body")

     (match args
       [(list id)
	(post-and-print "fetch-post-body" `((id . ,id)))]

       [(list id filename)
	(if (or (file-exists? filename) (directory-exists? filename))
	    (displayln (format "File ~a already exists. Please use a different one." filename))
	    (with-output-to-file filename
	      (lambda ()
		(post-and-print "fetch-post-body" `((id . ,id))))
	      #:exists 'replace))])]

    [(string=? command "update-post")
     (match args
       [(list id  text-file)
    	(if (not (file-exists? text-file))
    	    (displayln (format "Error: File ~a does not exist." text-file))
    	    (post-and-print "update-post" `((id . ,id)
					    (body . ,(file->string text-file)))))]
       [(list id text-file title)
	(if (not (file-exists? text-file))
    	    (displayln (format "Error: File ~a does not exist." text-file))
    	    (post-and-print "update-post" `((id . ,id)
					    (title . ,title)
					    (body . ,(file->string text-file)))))])]

    [(string=? command "rm-post")
     (let* ([id (car args)])
       (if id
	   (post-and-print  "rm-post" `((id . ,id)))	   
    	   (displayln "Error: id must be a number")
	   ))]

    [(string=? command "publish-preview")
     (let* ([id (car args)])
       (if id
	   (post-and-print  "publish-preview" `((id . ,id)))	   
    	   (displayln "Error: id must be a number")	   
    	   ))]

    [else (raise-user-error (format "Command ~a not found" command))]
    
    )
   
   (run-command)))
