#lang scribble/manual

@require[@for-label[blogo/updateapi
                    racket/base
		    racket/contract
		    ]]

@title{blogo/updateapi}


@defmodule[blogo/updateapi]

This module provides an API for updating a blog. It has functions to
add, remote and edit existing posts. It also has a function to preview
a new post provided the contents.

@defparam[current-blog a-blog (or/c blog? #f)]{ A parameter that
defines the blog to use for the API functions. If it has value
@racket[#f] it means that no blog has been assigned yet.}

@defproc[(set-current-blog! [a-blog (or/c string? blog?)])
			    void?]{
			    
Changes the @racket[current-blog] parameter according to the value
provided by @racket[a-blog]. If @racket[a-blog] is a string, it should
be a path to a already initialized blog; the blogo structure is
created using that path and it is what is set as the value of
@racket[current-blog].}

@defproc[(create-preview-table ) void?]{Creates the table
PREVIEWS in the database of the parameter @racket[current-blog]. The
schema is (ID INTEGER PRIMARY KEY,TITLE TEXT,BODY TEXT).}

@defproc[(add-preview [title string?] [body string?]) void?]{Adds a
preview to the PREVIEWS table with the given title and body.
}

@defproc[(rm-preview [id integer?]) void?]{Removes the preview with
the provided id from the PREVIEWS table.}

@defproc[(list-previews) string?]{Returns a json string. It contains a list of
pairs. The first element of each pair is the id of a preview and the second
element is the corresponding title.}

@defproc[(get-preview [id integer?]) (values string? string?)]{Returns
the title and body corresponding to the provided id.}

@section{URLs for API}

@itemlist[
@item{@italic{/add-preview} should be requested with the POST method and contain the variables @bold{title} and @bold{body}.}
@item{@italic{/list-previews} should be requested with the GET method. It returns the result of a call to @racket[list-previews].}
]
