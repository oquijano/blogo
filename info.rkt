#lang info

(define collection "blogo")

(define deps '("base"
               "rackunit-lib"
	       "yaml"
	       ))

(define racket-launcher-libraries '("commandline_program.rkt" "commandline_client_program.rkt"))
(define racket-launcher-names '("blogo" "blogo-client"))

(define build-deps '("scribble-lib" "racket-doc"))
(define scribblings '(("scribblings/blogo.scrbl" ())))
		       
(define pkg-desc "Description Here")
(define version "0.9.2")
(define pkg-authors '(oscar))
