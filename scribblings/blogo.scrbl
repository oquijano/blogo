#lang scribble/manual

@require[@for-label[blogo
                    racket/base]]

@title{blogo}
@author{oscar}

@defmodule[blogo]

@section{Blog languages}
The blog comes with support for several languages.

In the database file there is a table called
@emph{available_languages} that contains the list of languages
available for the blog.

With the browser the language is selected with the @emph{lang} query.

@defproc[(selected-language [req request?])  (or/c string? #f)]{ If the
	@emph{lang} query exists in the url, it returns its
	value. Otherwise it returns @racket[#f]
	}

@include-section["updateapi.scrbl"]